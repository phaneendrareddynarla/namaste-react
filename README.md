# Namaste React  


# Parcel
- Dev Build
- Local Server
- HMR = Hot Module Replacement => I will build as soon as we make changes in any files because of
- File Watching Algorithm - written in C++
- Caching - Faster builds 
- Image Optimization 
- Minification
- Bundling
- Compress
- Consistent Hashing
- Code Splitting
- Differentrial Bundling - support older browsers
- Diagnostics 
- Error Handling
- HTTPs
- Tree Shaking - remove unsed code
- Different dev and prod bundles => For prod use command -> npx parcel build index.html