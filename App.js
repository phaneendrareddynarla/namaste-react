import React from "react"
import ReactDOM from "react-dom/client"

//react element => Object
const heading = React.createElement("h1", {id : "heading"}, "Namaste React!");
console.log(heading)

//JSX => Babble converts JSX code to ReactElement
const jsxHeading = (<h1 className="head" tabIndex="5">
                        Namste React using React Element!</h1>);
console.log(jsxHeading)

const age = 28;

//React Functional Component

//we can aslo create the functional component as below
const HeadingComponent2 = () => {
    return <><h2>Component Composition!</h2></>
}

//we can also load ReactElement inside the Functional Component using {}
//we can also load another component instead component by using <> - This is also called as Component Composition
const HeadingComponent = () => (
<div id="container">
    {jsxHeading} 
    <h1>Namaste React Functional Component!</h1>
    <HeadingComponent2></HeadingComponent2>
</div>);



//root.render => converts all these ReactElement Object into browser understandable HTML
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<HeadingComponent />);